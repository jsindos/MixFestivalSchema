import Postmeta from './postmeta'
import User from './user'
import Thumbnail from './thumbnail'
import FilmInformation from './acf/filmInformation'
import MemberInformation from './acf/memberInformation'
import LandingPage from './acf/landingPage'
import Archive from './acf/archive'
import SponsorInformation from './acf/sponsorInformation'

const Post = `
  scalar JSON

  type Post {
    id: Int
    post_title: JSON
    post_content: JSON
    post_excerpt: String
    post_status: String
    post_type: String
    post_name: String
    post_parent: Int
    post_date: String
    menu_order: Int
    layout: Postmeta
    thumbnail: Thumbnail
    categories: [Category]
    post_meta(keys: [MetaType], after: String, first: Int, before: String, last: Int): [Postmeta]
    author: User,
    parent_event: Post,
    dates: [JSON],
    film_information: FilmInformation,
    member_information: MemberInformation,
    landingPage: LandingPage,
    aboutPageImages: [JSON],
    archive: Archive,
    sponsor_information: SponsorInformation
  }
`

export default () => [Post, Postmeta, User, FilmInformation, MemberInformation, LandingPage, Archive, SponsorInformation, ...Thumbnail]