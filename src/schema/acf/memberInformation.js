const MemberInformation = `
  scalar JSON

  type MemberInformation {
    email: JSON
    position: JSON
  }
`

export default () => [MemberInformation]
