const FilmInformation = `
  scalar JSON

  type FilmInformation {
    director: JSON
    year: JSON
    language: JSON
    subtitles: JSON
    trailer: JSON
    length: JSON
  }
`

export default () => [FilmInformation]
