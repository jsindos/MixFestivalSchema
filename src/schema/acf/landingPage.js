const LandingPage = `
  scalar JSON

  type LandingPage {
    aboutPageBlurb: JSON
    mixProgramDownload: JSON
    mixProgramImage: JSON
    mixProgramBlurb: JSON
  }
`

export default () => [LandingPage]
