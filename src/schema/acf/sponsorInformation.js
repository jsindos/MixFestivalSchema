const SponsorInformation = `
  scalar JSON

  type SponsorInformation {
    sponsorCategory: JSON
  }
`

export default () => [ SponsorInformation ]
