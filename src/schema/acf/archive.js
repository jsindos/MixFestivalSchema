const Archive = `
  scalar JSON

  type Archive {
    year: JSON
    link: JSON
  }
`

export default () => [Archive]
