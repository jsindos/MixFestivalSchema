export default function WordExpressResolvers (Connectors, publicSettings) {
  const Resolvers = {
    Query: {
      settings () {
        return publicSettings
      },
      category (_, { term_id, name }) {
        return Connectors.getTerm(term_id, name)
      },
      posts (_, args) {
        return Connectors.getPosts(args)
      },
      post (_, { name, id, post_type, language }) {
        return Connectors.getPost(id, name, post_type, language)
      },
      postmeta (_, { post_id, keys }) {
        return Connectors.getPostmeta(post_id, keys)
      },
      menus (_, { name }) {
        return Connectors.getMenu(name)
      },
      user (_, { id, name }) {
        return Connectors.getUser({ id, name })
      },
      attachments (_, { ids }) {
        return Connectors.getThumbnails(ids)
      },
      films (_, { language }) {
        return Connectors.getFilms(language)
      },
      events (_, { language }) {
        return Connectors.getEvents(language)
      }
    },
    Category: {
      posts (category, args) {
        return Connectors.getTermPosts(category.term_id, args)
      }
    },
    Post: {
      layout (post) {
        return Connectors.getPostLayout(post.id)
      },
      post_meta (post, keys) {
        return Connectors.getPostmeta(post.id, keys)
      },
      thumbnail (post) {
        return Connectors.getPostThumbnail(post.id)
      },
      author (post) {
        return Connectors.getUser({ userId: post.post_author })
      },
      categories (post) {
        return Connectors.getPostTerms(post.id)
      },
      post_content (post) {
        return Connectors.getPostContent(post)
      },
      post_title (post) {
        return Connectors.getPostTitle(post)
      },
      // No longer used
      parent_event (post) {
        return Connectors.getParentEvent(post.id)
      },
      // Repeater fields go straight to their respective connector
      dates (post) {
        // Get appropriate post based on language here
        // Always get english version so we do nothing
        return Connectors.getDates(post.id)
      },
      // All other fields go through `getFilmInformation`
      film_information (post) {
        return Connectors.getFilmInformation(post)
      },
      member_information (post) {
        return Connectors.getFilmInformation(post)
      },
      sponsor_information (post) {
        return Connectors.getFilmInformation(post)
      },
      landingPage (post) {
        return Connectors.getFilmInformation(post)
      },
      // Repeater fields go straight to their respective connector
      aboutPageImages (post) {
        // Get appropriate post based on language here
        return Connectors.getAboutPageImages(post.id)
      },
      archive (post) {
        return Connectors.getFilmInformation(post)
      }
    },
    Postmeta: {
      connecting_post (postmeta) {
        return Connectors.getPost(postmeta.meta_value)
      }
    },
    Menu: {
      items (menu) {
        return menu.items
      }
    },
    MenuItem: {
      navitem (menuItem) {
        return Connectors.getPost(menuItem.linkedId)
      },
      children (menuItem) {
        return menuItem.children
      }
    },
    User: {
      posts (user, args) {
        const a = {
          ...args,
          userId: user.id
        }
        return Connectors.getPosts(a)
      }
    },
    FilmInformation: {
      director (post) {
        return Connectors.getDirector(post.id)
      },
      year (post) {
        return Connectors.getYear(post.id)
      },
      language (post) {
        return Connectors.getLanguage(post.id)
      },
      subtitles (post) {
        return Connectors.getSubtitles(post)
      },
      trailer (post) {
        return Connectors.getTrailer(post.id)
      },
      length (post) {
        return Connectors.getLength(post.id)
      }
    },
    MemberInformation: {
      email (post) {
        return Connectors.getEmail(post.id)
      },
      position (post) {
        return Connectors.getPosition(post.id)
      }
    },
    LandingPage: {
      aboutPageBlurb (post) {
        return Connectors.getAboutPageBlurb(post)
      },
      mixProgramDownload (post) {
        return Connectors.getMixProgramDownload(post.id)
      },
      mixProgramImage (post) {
        return Connectors.getMixProgramImage(post.id)
      },
      mixProgramBlurb (post) {
        return Connectors.getMixProgramBlurb(post)
      }
    },
    Archive: {
      year (post) {
        return Connectors.getArchiveYear(post.id)
      },
      link (post) {
        return Connectors.getLink(post.id)
      }
    },
    SponsorInformation: {
      sponsorCategory (post) {
        return Connectors.getSponsorCategory(post.id)
      }
    }
  }

  return Resolvers
}
