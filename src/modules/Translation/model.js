import Sequelize from 'sequelize'

export default function (Conn, prefix) {
  return Conn.define(prefix + 'icl_translations', {
    translation_id: { type: Sequelize.INTEGER, primaryKey: true },
    element_type: { type: Sequelize.STRING },
    element_id: { type: Sequelize.INTEGER },
    trid: { type: Sequelize.INTEGER },
    language_code: { type: Sequelize.STRING },
    source_language_code: { type: Sequelize.STRING }
  })
}