const Sequelize = require('sequelize')

module.exports = function (Conn, prefix) {
  return Conn.define(prefix + 'term_taxonomy', {
    term_taxonomy_id: { type: Sequelize.INTEGER, primaryKey: true },
    taxonomy: { type: Sequelize.STRING },
    description: { type:Sequelize.STRING }
  })
}
