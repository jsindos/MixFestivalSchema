import Sequelize from 'sequelize'

const Op = Sequelize.Op

// Repeater field with subkey image
export default function (Post, Postmeta) {
  return function (postId) {
    return Post.findAll({
      where: {
        post_type: 'acf-field'
      }
    }).then(posts => {
      return Postmeta.findAll({
        where: {
          post_id: postId
        }
      }).then(postMetas => {
        if (postMetas.length > 0) {
          // Map the postMetas to their ACF fields, filtering those that don't have a match
          // Those remaining are instances of a repeater field group
          return postMetas.filter(pm => posts.map(p => p.post_name).includes(pm.meta_value))
            .map(pm => ({
              ...pm.dataValues,
              key: posts.find(p => p.post_name === pm.meta_value).post_excerpt,
              value: postMetas.find(ppm => ppm.meta_key === pm.meta_key.substring(1))
                && postMetas.find(ppm => ppm.meta_key === pm.meta_key.substring(1)).meta_value,
              index: pm.meta_key.match(/^_images_(\d{1})/) && Number(pm.meta_key.match(/^_images_(\d{1})/)[1])
            }))
            // Filter those that didn't have an index appearing in the name of the meta_key
            .filter(pm => Number.isInteger(pm.index))
            .reduce((accumulator, currentValue) => {
              const group = accumulator.find(a => a.index === currentValue.index) || { index: currentValue.index }
              const newGroup = { ...group, [currentValue.key]: currentValue.value }
              return accumulator.filter(a => a.index !== currentValue.index).concat([ newGroup ])
            }, [])
            .sort((a, b) => a.index > b.index)
        }
      }).then(aboutPageImages => {
        return Post.findAll({
          where: {
            id: {
              [Op.in]: aboutPageImages.map(i => Number(i.image))
            }
          }
        }).then(posts => {
          // `image` should match the key of the repeater subfield which is an Image field
          return aboutPageImages.map(i => ({ ...i, image: posts.find(p => p.id === Number(i.image)).guid }))
        })
      })
    })
  }
}