import getField from './util/getField'

export default function (Post, Postmeta) {
  return function (post) {
    console.log(post.dataValues)
    return Post.findOne(getField('subtitles')).then(post => {
      if (!post) return null
      return Postmeta.findOne({
        where: {
          meta_value: post.post_name
        }
      })
    }).then(postMeta => {
      return Promise.all([
        Postmeta.findOne({
          where: {
            meta_key: postMeta.meta_key.substring(1),
            post_id: post.id
          }
        }),
        post.dataValues.translationPostId
          ? Postmeta.findOne({
            where: {
              meta_key: postMeta.meta_key.substring(1),
              post_id: post.dataValues.translationPostId
            }
          })
          : new Promise(resolve => resolve())
      ]).then(([ enSubtitle, daSubtitle ]) => ({
        en: enSubtitle && enSubtitle.meta_value,
        da: daSubtitle && daSubtitle.meta_value
      }))
    })
  }
}
