export default (fieldName) => ({
  where: {
    post_type: 'acf-field',
    post_excerpt: fieldName
  }
})