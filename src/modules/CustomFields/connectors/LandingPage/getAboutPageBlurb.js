import Sequelize from 'sequelize'
const Op = Sequelize.Op

import getField from '../util/getField'

export default function (Post, Postmeta) {
  return function (post) {
    return Post.findOne(getField('about_page_blurb')).then(p => {
      if (!p) return null
      return Postmeta.findOne({
        where: {
          meta_value: p.post_name
        }
      })
    }).then(postMeta => {
      return Postmeta.findAll({
        where: {
          meta_key: postMeta.meta_key.substring(1),
          post_id: {
            [Op.in]: post.dataValues.translationPostId ? [ post.dataValues.translationPostId, post.id ] : [ post.id ]
          }
        }
      })
    }).then(postMetas => ({
      en: postMetas && postMetas.find(p => p.post_id === post.id).meta_value,
      da: postMetas && post.dataValues.translationPostId && postMetas.find(p => p.post_id === post.dataValues.translationPostId).meta_value,
    }))
  }
}

