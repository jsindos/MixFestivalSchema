import getField from './util/getField'

// const FIELDS = ['director', 'language', 'subtitles', 'year']

export default function (Post, Postmeta, Translation) {
  return function (post) {
    // console.log(post.dataValues.selectedLanguage)
    return post
    // return Translation.findAll({
    //   where: {
    //     trid: post.id
    //   }
    // })
    // .then(translations => translations.filter(t => t.source_language_code))
    // .then(filteredTranslations => {
    //   // console.log(filteredTranslations.length)
    //   if (filteredTranslations.length) {
    //     return Post.findOne({
    //       where: {
    //         id: filteredTranslations[0].element_id
    //       }
    //     })
    //   }
    //   return null
    // }).then(danishTranslationPost => {post.dataValues.danishTranslationPost = danishTranslationPost; return post})
    // .then(translations => console.log(`TRANSLATIONS: ${post.id}`, translations.map(t => t.dataValues)))

    // Post.findAll({
    //   where: {
    //     post_type: 'acf-field',
    //     post_excerpt: fieldName
    //   }
    // }).then(posts => {
    //   return Postmeta.findAll({
    //     where: {
    //       post_id: postId
    //     }
    //   }).then(postMetas => {
    //     const fields = posts.filter(p => FIELDS.includes(p.post_excerpt))
    //     const linkingMetas = postMetas.filter(pm => fields.map(f => f.post_name).includes(pm.meta_value))
    //     return postMetas.filter(pm => linkingMetas.map(l => l.meta_key.substring(1)).includes(pm.meta_key))
    //   })
    // })
    /**
     * Old style to get a single field
     */
    // Post.findOne(getField('director')).then(post => {
    //   return Postmeta.findOne({
    //     where: {
    //       meta_value: post.post_name
    //     }
    //   })
    // }).then(postMeta => {
    //   return Postmeta.findOne({
    //     where: {
    //       meta_key: postMeta.meta_key.substring(1),
    //       post_id: postId
    //     }
    //   })
    // }).then(postMeta => postMeta.meta_value)
  }
}