import getDates from './getDates'
import getDirector from './getDirector'
import getLanguage from './getLanguage'
import getSubtitles from './getSubtitles'
import getYear from './getYear'
import getFilmInformation from './getFilmInformation'
import getTrailer from './getTrailer'
import getLength from './getLength'
import getEmail from './getEmail'
import getPosition from './getPosition'

// Landing Page
import getAboutPageBlurb from './LandingPage/getAboutPageBlurb'
import getMixProgramDownload from './LandingPage/getMixProgramDownload'
import getMixProgramImage from './LandingPage/getMixProgramImage'
import getMixProgramBlurb from './LandingPage/getMixProgramBlurb'

// About Page Images
import getAboutPageImages from './AboutPageImages/getAboutPageImages'

// Archive Page
import { default as getArchiveYear } from './Archive/getYear'
import getLink from './Archive/getLink'

// Sponsors
import getSponsorCategory from './getSponsorCategory'

export default function ({Post, Postmeta, Translation}, settings) {
  return {
    getDates: getDates(Post, Postmeta),
    getDirector: getDirector(Post, Postmeta),
    getLanguage: getLanguage(Post, Postmeta),
    getSubtitles: getSubtitles(Post, Postmeta),
    getYear: getYear(Post, Postmeta),
    getFilmInformation: getFilmInformation(Post, Postmeta, Translation),
    getTrailer: getTrailer(Post, Postmeta),
    getLength: getLength(Post, Postmeta),
    getEmail: getEmail(Post, Postmeta),
    getPosition: getPosition(Post, Postmeta),
    // Landing Page
    getAboutPageBlurb: getAboutPageBlurb(Post, Postmeta),
    getMixProgramDownload: getMixProgramDownload(Post, Postmeta),
    getMixProgramImage: getMixProgramImage(Post, Postmeta),
    getMixProgramBlurb: getMixProgramBlurb(Post, Postmeta),
    // About Page Images
    getAboutPageImages: getAboutPageImages(Post, Postmeta),
    // Archive Page
    getArchiveYear: getArchiveYear(Post, Postmeta),
    getLink: getLink(Post, Postmeta),
    // Sponsors
    getSponsorCategory: getSponsorCategory(Post, Postmeta)
  }
}