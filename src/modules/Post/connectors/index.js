import getPost from './getPost'
import getPosts from './getPosts'
import getPostTerms from './getPostTerms'
import getTermPosts from './getTermPosts'
import getPostLayout from './getPostLayout'
import getParentEvent from './getParentEvent'
import getFilms from './getFilms'
import getEvents from './getEvents'

// Translation fields
import getPostContent from './getPostContent'
import getPostTitle from './getPostTitle'

export default function ({Post, Postmeta, Terms, TermRelationships, Translation, TermTaxonomy}, settings) {
  return {
    getPost: getPost(Post, TermTaxonomy),
    getPosts: getPosts(Post, TermTaxonomy),
    getPostTerms: getPostTerms(Terms, TermRelationships, settings),
    getTermPosts: getTermPosts(TermRelationships, Post, settings),
    getPostLayout: getPostLayout(Postmeta),
    getParentEvent: getParentEvent(Post, Postmeta),
    getFilms: getFilms(Post, TermTaxonomy),
    getEvents: getEvents(Post, TermTaxonomy),
    getPostContent: getPostContent(Post),
    getPostTitle: getPostTitle(Post)
  }
}