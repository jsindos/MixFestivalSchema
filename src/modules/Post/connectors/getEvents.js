import Sequelize from 'sequelize'
const Op = Sequelize.Op

export default function (Post, TermTaxonomy) {
  return function (language) {
    const orderBy = ['menu_order', 'ASC']
    const where = {
      post_status: 'publish',
      post_type: {
        [Op.in]: ['wp_event']
      }
    }

    // Need to get the danish translation postId and filter out the danish translations
    return Post.findAll({
      where: where
    }).then(posts => {
      let translationIds = []
      return posts.reduce((promise, post) => {
        return promise
          .then(result => {
            return TermTaxonomy.findOne({
              where: {
                description: {
                  [Op.like]: `%"da";i:${post.id}%`
                },
                taxonomy: 'post_translations'
              }
            })
          }).then(result =>
            result &&
            translationIds.push({
              danishId: post.id,
              englishId: Number(result.description.match(/"en";i:(\d+)/)[1])
            }))
      }, Promise.resolve())
        .then(() => {
          const danishIds = translationIds.map(t => t.danishId)
          return posts.filter(post => !danishIds.includes(post.id))
            .map(p => {
              p.dataValues.translationPostId = translationIds.find(f => f.englishId === p.id) && translationIds.find(f => f.englishId === p.id).danishId
              return p
            })
        })
    })
  }
}
