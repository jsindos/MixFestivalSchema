export default function (Post) {
  return function (post) {
    return post.dataValues.translationPostId
      ? Post.findOne({
        where: {
          id: post.dataValues.translationPostId
        }
      }).then(p => ({
        da: p && p.post_content,
        en: post.post_content
      }))
      : {
        en: post.post_content,
        da: ''
      }
  }
}